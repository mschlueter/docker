# Docker-Compose / Images for local development

## Apache with PHP

If you want a apache server with a specific php version you can copy the files of the *apache-php* directory into your project root directory.

Default: PHP 7.1

## Nginx with PHP-FPM

If you want a nginx server with a specific php version you can copy the files of the *nginx-php-fpm* directory into your project root directory.

Default: PHP 7.1